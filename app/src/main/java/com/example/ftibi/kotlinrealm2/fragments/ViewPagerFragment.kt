package com.example.ftibi.kotlinrealm2.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.example.ftibi.kotlinrealm2.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

class ViewPagerFragment : Fragment() {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager

    val fragmentList = ArrayList<Fragment>()
    val fragmentTitleList = ArrayList<String>()

    var selectedAnimal = "cat"

    override fun onStart() {
        super.onStart()

        setHasOptionsMenu(true)
        AddToolbar.addToolbar(activity, view, R.id.toolbar_viewpager_fragment, true, getString(R.string.animals))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_view_pager, container, false)

        val fabButton = activity.findViewById<FloatingActionButton>(R.id.floating_add_button_frame_activity)
        if (fabButton.visibility != View.VISIBLE) fabButton.visibility = View.VISIBLE
        fabButton.setOnClickListener {
            addDialog(selectedAnimal)
        }

        viewPager = v.findViewById(R.id.viewpager_viewpager_fragment)
        tabLayout = v.findViewById(R.id.tab_layout_viewpager_fragment)

        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> selectedAnimal = "cat"
                    1 -> selectedAnimal = "dog"
                }
            }
        })

        return v
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(CatFragment(), getString(R.string.cats))
        adapter.addFragment(DogFragment(), getString(R.string.dogs))

        viewPager.adapter = adapter
    }

    inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment = fragmentList[position]
        override fun getCount(): Int = fragmentList.size
        override fun getPageTitle(position: Int): CharSequence = fragmentTitleList[position]

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitleList.add(title)
        }
    }

    private fun addDialog(whichAnimal: String) {
        var result = "close"
        val rootView = LayoutInflater.from(context).inflate(R.layout.dialog_rename, null)
        val editText = rootView.findViewById<TextInputEditText>(R.id.text_rename_dialog)
        val alertDialogBuilder = AlertDialog.Builder(context)
                .setTitle(getString(R.string.add))
                .setView(rootView)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), { _, _ -> })
                .setPositiveButton(getString(R.string.add), { _, _ -> })
                .create()

        fun dismissDialog() {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken, 0)

            Handler().postDelayed({
                alertDialogBuilder.dismiss()
            }, 200)
        }

        alertDialogBuilder.show()
        alertDialogBuilder.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            dismissDialog()
        }

        val realmHelper = RealmHelper(Realm.getDefaultInstance())
        val positiveButton = alertDialogBuilder.getButton(AlertDialog.BUTTON_POSITIVE)
        positiveButton.setOnClickListener {
            if (result == "close") dismissDialog()
            else {
                if (result == "insert_dog") realmHelper.insertDog(editText.text.toString())
                else if (result == "insert_cat") realmHelper.insertCat(editText.text.toString())
                dismissDialog()
                Snackbar.make(activity.findViewById(
                        R.id.coordinator_layout_frame_activity),
                        R.string.animal_added,
                        Snackbar.LENGTH_SHORT).show()
            }
        }

        TextChangeObservable.createTextChangeObservable(editText)
                .observeOn(Schedulers.io())
                .map(Function<String, String> {
                    val helper = RealmHelper(Realm.getDefaultInstance())
                    if (editText.text.toString() == "") {
                        return@Function "close"
                    } else {
                        if (whichAnimal == "dog") {
                            if (helper.getDog(editText.text.toString()) == null) return@Function "insert_dog"
                            else return@Function "exists"
                        } else if (whichAnimal == "cat") {
                            if (helper.getCat(editText.text.toString()) == null) return@Function "insert_cat"
                            else return@Function "exists"
                        }
                    }
                    "error"
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { t ->
                    if (t == "exists") {
                        editText.error = getString(R.string.existing_name)
                        positiveButton.isEnabled = false
                    } else {
                        result = t
                        positiveButton.isEnabled = true
                    }
                }

        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> if (fragmentManager.backStackEntryCount > 0) fragmentManager.popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun newInstance(): ViewPagerFragment = ViewPagerFragment()
    }
}
