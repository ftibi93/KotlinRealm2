package com.example.ftibi.kotlinrealm2

import android.support.design.widget.TextInputEditText
import android.text.Editable
import android.text.TextWatcher
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import java.util.concurrent.TimeUnit

class TextChangeObservable {
    companion object {
        fun createTextChangeObservable(editText: TextInputEditText): Observable<String> {
            val textChangeObservable = Observable.create(ObservableOnSubscribe<String> { e ->
                val textWatcher = object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {}
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        e.onNext(s.toString())
                    }
                }
                editText.addTextChangedListener(textWatcher)

                e.setCancellable {
                    editText.removeTextChangedListener(textWatcher)
                }
            })
            return textChangeObservable
                    .debounce(500, TimeUnit.MILLISECONDS)
        }
    }
}
