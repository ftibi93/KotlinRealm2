package com.example.ftibi.kotlinrealm2.models

import io.realm.RealmObject

open class Cat : RealmObject() {
    var name: String? = null
}