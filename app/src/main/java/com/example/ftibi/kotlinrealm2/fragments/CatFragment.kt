package com.example.ftibi.kotlinrealm2.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.ftibi.kotlinrealm2.MakeSnackbarEvent
import com.example.ftibi.kotlinrealm2.RealmHelper
import com.example.ftibi.kotlinrealm2.R
import com.example.ftibi.kotlinrealm2.RxBus
import com.example.ftibi.kotlinrealm2.models.Cat
import io.realm.*

class CatFragment : Fragment() {

    lateinit var onRenameButtonClicked: OnRenameButtonClicked

    private lateinit var recyclerView: RecyclerView

    lateinit var realmHelper: RealmHelper
    lateinit var realm: Realm

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnRenameButtonClicked) onRenameButtonClicked = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_animals2, container, false)

        realm = Realm.getDefaultInstance()
        realmHelper = RealmHelper(realm)

        recyclerView = v.findViewById(R.id.recycler_view_animals)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        registerForContextMenu(recyclerView)

        recyclerView.adapter = Adapter2(realmHelper.getCatList())

        return v
    }

    inner class Adapter2(data: OrderedRealmCollection<Cat>?) : RealmRecyclerViewAdapter<Cat, Adapter2.ViewHolder>(data, true) {
        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindItems(this.getItem(position)!!)

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.cat_list_item, parent, false))
        }


        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener {
            init {
                itemView.setOnCreateContextMenuListener(this)
            }

            override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
                menu?.setHeaderTitle(cat.name)
                menu?.add(0, v!!.id, 0, getString(R.string.rename))?.setOnMenuItemClickListener {
                    onRenameButtonClicked.onRenameButtonClicked(cat.name!!, "cat")
                    true
                }
                menu?.add(0, v!!.id, 0, getString(R.string.delete))?.setOnMenuItemClickListener {
                    realmHelper.deleteCat(cat.name!!)
                    RxBus.publish(MakeSnackbarEvent(getString(R.string.cat_deleted)))
                    true
                }
            }

            private lateinit var cat: Cat

            fun bindItems(cat: Cat) {
                this.cat = cat
                val catName = itemView.findViewById<TextView>(R.id.name_cat_list_item)

                catName.text = cat.name
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unregisterForContextMenu(recyclerView)
    }

    interface OnRenameButtonClicked {
        fun onRenameButtonClicked(name: String, whichAnimal: String)
    }

}
