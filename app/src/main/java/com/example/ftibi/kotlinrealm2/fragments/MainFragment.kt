package com.example.ftibi.kotlinrealm2.fragments

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import com.example.ftibi.kotlinrealm2.*
import com.example.ftibi.kotlinrealm2.R
import com.example.ftibi.kotlinrealm2.models.Person
import io.realm.*
import kotlin.properties.Delegates


class MainFragment : Fragment() {

    companion object {
        fun newInstance(): MainFragment = MainFragment()
    }

    private lateinit var recyclerView: RecyclerView

    lateinit var personListener: OnPersonSelected
    private lateinit var manageButtonListener: OnManageButtonClicked

    private var realm: Realm by Delegates.notNull()
    private var realmHelper: RealmHelper by Delegates.notNull()

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnPersonSelected) personListener = context
        if (context is OnManageButtonClicked) manageButtonListener = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_main, container, false)
        recyclerView = v.findViewById(R.id.person_recycler_view2)
        registerForContextMenu(recyclerView)

        realm = Realm.getDefaultInstance()
        realmHelper = RealmHelper(Realm.getDefaultInstance())

        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        recyclerView.adapter = Adapter(realmHelper.getPersonList())

        RxBus.publish(MainFragmentOpenEvent())

        return v
    }

    inner class Adapter(data: OrderedRealmCollection<Person>?) : RealmRecyclerViewAdapter<Person, Adapter.ViewHolder>(data, true) {
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.person_list_item, parent, false))
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindItems(this.getItem(position)!!)
            holder.itemView.setOnClickListener {
                personListener.onPersonItemSelected(this.getItem(position)!!.id)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnCreateContextMenuListener {
            init {
                view.setOnCreateContextMenuListener(this)
            }

            private lateinit var person: Person

            override fun onCreateContextMenu(p0: ContextMenu, p1: View, p2: ContextMenu.ContextMenuInfo?) {
                p0.setHeaderTitle(person.name)
                p0.add(0, p1.id, 0, getString(R.string.delete)).setOnMenuItemClickListener {
                    realmHelper.deletePerson(person.name)
                    RxBus.publish(MakeSnackbarEvent(getString(R.string.person_deleted)))
                    true
                }
            }

            fun bindItems(person: Person) {
                this.person = person
                val personName = itemView.findViewById<TextView>(R.id.name_person_list_item)
                val personAge = itemView.findViewById<TextView>(R.id.age_person_list_item)
                val personDog = itemView.findViewById<TextView>(R.id.dog_person_list_item)
                val personCats = itemView.findViewById<TextView>(R.id.cats_person_list_item)

                fun setText(view: TextView) {
                    if (view.id == R.id.dog_person_list_item) view.text = itemView.context.getString(R.string.no_dog)
                    else if (view.id == R.id.cats_person_list_item) view.text = itemView.context.getString(R.string.no_cats)

                    view.setTypeface(view.typeface, Typeface.ITALIC)
                    view.alpha = 0.6f
                }

                personName.text = person.name
                personAge.text = person.age.toString()
                personCats.text = person.cats.toString()

                if (person.dog?.name == null) setText(personDog)
                else personDog.text = person.dog?.name

                val catCount = person.cats?.count()
                if (catCount == null || catCount == 0) setText(personCats)
                else personCats.text = itemView.resources.getString(R.string.count_of_cats, person.cats!!.count())
            }
        }
    }

    override fun onStart() {
        super.onStart()

        setHasOptionsMenu(true)
        AddToolbar.addToolbar(activity, view, R.id.toolbar_main_fragment, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        realm.close()
        unregisterForContextMenu(recyclerView)
    }

    interface OnPersonSelected {
        fun onPersonItemSelected(id: Int)
    }

    interface OnManageButtonClicked {
        fun onManageButtonClicked()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.manage_main_fragment -> manageButtonListener.onManageButtonClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater?.inflate(R.menu.menu_main_fragment, menu)
    }

}