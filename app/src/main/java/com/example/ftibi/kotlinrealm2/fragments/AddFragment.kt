package com.example.ftibi.kotlinrealm2.fragments

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.RelativeLayout
import com.example.ftibi.kotlinrealm2.*
import com.example.ftibi.kotlinrealm2.models.Cat
import io.realm.Realm
import io.realm.RealmList

class AddFragment : Fragment() {

    companion object {
        fun newInstance(): Fragment = AddFragment()
    }

    private lateinit var realmHelper: RealmHelper

    private lateinit var dogButton: Button
    private lateinit var catButton: Button
    private lateinit var cancelButton: Button
    private lateinit var saveButton: Button
    private lateinit var nameText: TextInputEditText
    private lateinit var ageText: TextInputEditText

    private lateinit var baseRelativeLayout: RelativeLayout

    private var chosenCats: ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_add, container, false)
        initViews(v)

        RxBus.publish(HideButtonEvent())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val colorAnimation = ValueAnimator.ofObject(
                    ArgbEvaluator(),
                    resources.getColor(R.color.colorTransitionStart, null),
                    resources.getColor(R.color.colorTransitionEnd, null))
            colorAnimation.duration = 250
            colorAnimation.addUpdateListener({ animation: ValueAnimator? ->
                baseRelativeLayout.setBackgroundColor(animation?.animatedValue as Int)
            })
            colorAnimation.startDelay = 300
            colorAnimation.start()
        }

        baseRelativeLayout.setOnClickListener { popFragment() }

        realmHelper = RealmHelper(Realm.getDefaultInstance())

        dogButton.setOnClickListener { dogDialog() }
        catButton.setOnClickListener { catDialog() }
        cancelButton.setOnClickListener { popFragment() }

        saveButton.setOnClickListener {
            var isExist = false
            var isNameEmpty = false
            var isAgeEmpty = false
            var isAgeInt = false

            if (realmHelper.getPersonByName(nameText.text.toString()) != null) {
                nameText.error = getString(R.string.existing_name)
            } else isExist = true
            if (nameText.text.toString() == "") nameText.error = getString(R.string.empty_name)
            else isNameEmpty = true
            if (ageText.text.toString() == "") ageText.error = getString(R.string.empty_age)
            else isAgeEmpty = true
            if (!ageText.text.toString().matches("^\\d+$".toRegex())) ageText.error = getString(R.string.age_must_be_whole)
            else isAgeInt = true

            if (isAgeEmpty && isExist && isNameEmpty && isAgeInt) {
                addToDatabase()
                popFragment()
            }
        }

        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
        RxBus.publish(MainFragmentOpenEvent())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val colorAnimation = ValueAnimator.ofObject(
                    ArgbEvaluator(),
                    resources.getColor(R.color.colorTransitionEnd, null),
                    resources.getColor(R.color.colorTransitionStart, null))
            colorAnimation.duration = 150
            colorAnimation.addUpdateListener({ animation: ValueAnimator? ->
                baseRelativeLayout.setBackgroundColor(animation?.animatedValue as Int)
            })
            colorAnimation.start()
        }
    }

    private fun popFragment() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        if (fragmentManager.backStackEntryCount > 0) fragmentManager.popBackStack()
    }

    private fun initViews(v: View) {
        baseRelativeLayout = v.findViewById(R.id.base_layout_add_fragment)
        dogButton = v.findViewById(R.id.dog_button_add_fragment)
        catButton = v.findViewById(R.id.cats_button_add_fragment)
        cancelButton = v.findViewById(R.id.cancel_button_add_fragment)
        saveButton = v.findViewById(R.id.save_button_add_fragment)
        nameText = v.findViewById(R.id.person_name_add_fragment)
        ageText = v.findViewById(R.id.person_age_add_fragment)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> popFragment()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun dogDialog() {
        val dogList = realmHelper.getDogList()
        val list: Array<String> = Array(dogList.size, { "" })
        for (index in dogList.indices) list[index] = dogList[index].name.toString()

        val builder = AlertDialog.Builder(context)
                .setTitle(getString(R.string.choose_a_dog))
                .setItems(list, { _, which -> dogButton.text = dogList[which].name })
                .create()
        builder.show()
    }

    private fun catDialog() {
        val catList = realmHelper.getCatList()
        val list: Array<String> = Array(catList.size, { "" })
        for (i in catList.indices) list[i] = catList[i].name.toString()

        val checkList: BooleanArray = kotlin.BooleanArray(catList.size, { false })
        catList.indices
                .filter { chosenCats.contains(catList[it].name) }
                .forEach { checkList[it] = true }

        val tempChosenCats: ArrayList<String> = ArrayList(chosenCats)

        val builder = AlertDialog.Builder(context)
                .setTitle(getString(R.string.choose_your_cats))
                .setMultiChoiceItems(list, checkList, { _, which, isChecked ->
                    run {
                        if (isChecked && !tempChosenCats.contains(list[which])) tempChosenCats.add(list[which])
                        if (!isChecked && tempChosenCats.contains(list[which])) tempChosenCats.remove(list[which])
                    }
                })
                .setPositiveButton(getString(R.string.OK), { dialog, _ ->
                    run {
                        chosenCats = ArrayList(tempChosenCats)
                        dialog.dismiss()
                    }
                })
                .setNegativeButton(getString(R.string.cancel), { dialog, _ -> dialog.cancel() })
                .setCancelable(true)
                .create()
        builder.show()
    }

    private fun addToDatabase() {
        val cats: RealmList<Cat>? = RealmList()
        for (cat in chosenCats) cats?.add(realmHelper.getCat(cat))

        realmHelper.insertPerson(
                nameText.text.toString(),
                ageText.text.toString().toInt(),
                realmHelper.getDog(dogButton.text.toString()),
                cats
        )
        RxBus.publish(MakeSnackbarEvent(getString(R.string.person_added)))
    }
}