package com.example.ftibi.kotlinrealm2

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.ftibi.kotlinrealm2.fragments.*
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.concurrent.TimeUnit

class FrameActivity : AppCompatActivity(),
        MainFragment.OnPersonSelected,
        MainFragment.OnManageButtonClicked,
        CatFragment.OnRenameButtonClicked,
        DogFragment.OnRenameButtonClicked {

    private lateinit var realmHelper: RealmHelper
    private lateinit var whichAnimal: String

    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var addButton: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)

        realmHelper = RealmHelper(Realm.getDefaultInstance())

        coordinatorLayout = findViewById(R.id.coordinator_layout_frame_activity)
        addButton = findViewById(R.id.floating_add_button_frame_activity)
        addButton.setOnClickListener {
            onAddPersonButtonClicked()
        }

        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.frame_layout, MainFragment.newInstance(), "personList")
                    .commit()
        }

        RxBus.listen(MainFragmentOpenEvent::class.java).subscribe({
            setButtonVisibility(true)
            addButton.setOnClickListener { onAddPersonButtonClicked() }
        })

        RxBus.listen(String::class.java).subscribe({ whichAnimal = it })
        RxBus.listen(HideButtonEvent::class.java).subscribe({ setButtonVisibility(false) })

        RxBus.listen(MakeSnackbarEvent::class.java).subscribe({
            makeSnackbar(it.text)
        })
    }

    private fun makeSnackbar(text: String) = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_SHORT).show()

    private fun setButtonVisibility(visibility: Boolean) {
        if (visibility) addButton.visibility = View.VISIBLE
        if (!visibility) addButton.visibility = View.GONE
    }

    override fun onPersonItemSelected(id: Int) {
        changeFragment(DetailsFragment.newInstance(id), "personDetails")
    }

    private fun onAddPersonButtonClicked() {
        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.enter_fragment_anim,
                        R.anim.exit_fragment_anim,
                        R.anim.pop_enter_fragment_anim,
                        R.anim.pop_exit_fragment_anim
                )
                .add(R.id.frame_layout, AddFragment.newInstance(), "addPerson")
                .addToBackStack(null)
                .commit()
    }

    override fun onManageButtonClicked() {
        changeFragment(ViewPagerFragment.newInstance(), "manageAnimals")
    }

    override fun onRenameButtonClicked(name: String, whichAnimal: String) {
        renameDialog(name, whichAnimal)
    }

    private fun renameDialog(name: String, whichAnimal: String) {
        var result = ""
        val layoutInflater = LayoutInflater.from(this)
        val rootView = layoutInflater.inflate(R.layout.dialog_rename, null)
        val editText = rootView.findViewById<TextInputEditText>(R.id.text_rename_dialog)
        val alertDialogBuilder = AlertDialog.Builder(this)
                .setTitle(getString(R.string.rename))
                .setView(rootView)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), { _, _ -> })
                .setPositiveButton("OK", { _, _ -> })
                .create()

        editText.setText(name, null)
        editText.setSelection(name.length)

        fun dismissDialog() {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken, 0)

            Handler().postDelayed({
                alertDialogBuilder.dismiss()
            }, 200)
        }

        alertDialogBuilder.show()
        val positiveButton = alertDialogBuilder.getButton(AlertDialog.BUTTON_POSITIVE)
        positiveButton.setOnClickListener {
            if (result == "change_dog_name") realmHelper.changeDogName(name, editText.text.toString())
            else if (result == "change_cat_name") realmHelper.changeCatName(name, editText.text.toString())
            dismissDialog()
        }
        alertDialogBuilder.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
            dismissDialog()
        }
        TextChangeObservable.createTextChangeObservable(editText)
                .observeOn(Schedulers.io())
                .map(io.reactivex.functions.Function<String, String> {
                    val helper = RealmHelper(Realm.getDefaultInstance())
                    if (whichAnimal == "dog") {
                        if (helper.getDog(editText.text.toString()) == null) return@Function "change_dog_name"
                        else return@Function "exists"
                    } else if (whichAnimal == "cat") {
                        if (helper.getCat(editText.text.toString()) == null) return@Function "change_cat_name"
                        else return@Function "exists"
                    }
                    "error"
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { t ->
                    if (t == "exists") {
                        positiveButton.isEnabled = false
                        editText.error = "Ez a név már létezik"
                    } else {
                        result = t
                        positiveButton.isEnabled = true
                    }
                }

        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    private fun changeFragment(fragment: Fragment, tag: String) {
        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.enter_fragment_anim,
                        R.anim.exit_fragment_anim,
                        R.anim.pop_enter_fragment_anim,
                        R.anim.pop_exit_fragment_anim
                )
                .replace(R.id.frame_layout, fragment, tag)
                .addToBackStack(null)
                .commit()
    }
}
