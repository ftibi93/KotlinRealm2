package com.example.ftibi.kotlinrealm2

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

//source: https://android.jlelse.eu/super-simple-event-bus-with-rxjava-and-kotlin-f1f969b21003
object RxBus {

    private val publisher = PublishSubject.create<Any>()

    fun publish(event: Any) {
        publisher.onNext(event)
    }

    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)

}