package com.example.ftibi.kotlinrealm2

import com.example.ftibi.kotlinrealm2.models.Cat
import com.example.ftibi.kotlinrealm2.models.Dog
import com.example.ftibi.kotlinrealm2.models.Person
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmResults

class RealmHelper(val realm: Realm) {

    fun insertPerson(
            name: String,
            age: Int,
            dog: Dog? = null,
            cats: RealmList<Cat>? = null
    ) {
        var id = 0
        if (realm.where(Person::class.java).max("id") != null) {
            id = realm.where(Person::class.java).max("id").toString().toInt() + 1
        }

        realm.executeTransaction {
            val person = realm.createObject(Person::class.java, id)
            person.name = name
            person.age = age
            person.dog = dog
            person.cats = cats
        }
    }

    fun getPersonList(): RealmResults<Person>? = realm.where(Person::class.java).findAllSorted("name")

    fun insertDog(name: String) {
        val dogCount = realm.where(Dog::class.java).equalTo("name", name).count().toInt()

        if (dogCount == 0) {
            realm.executeTransaction {
                val dog = realm.createObject(Dog::class.java)
                dog.name = name
            }
        }
    }

    fun insertCat(name: String) {
        val catCount = realm.where(Cat::class.java).equalTo("name", name).count().toInt()

        if (catCount == 0) {
            realm.executeTransaction {
                val cat = realm.createObject(Cat::class.java)
                cat.name = name
            }
        }
    }

    fun getDog(name: String): Dog? {
        return realm.where(Dog::class.java).equalTo("name", name).findFirst()
    }

    fun getDogList(): RealmResults<Dog> {
        return realm.where(Dog::class.java).findAll().sort("name")
    }

    fun deleteDog(name: String) {
        realm.executeTransaction {
            getDog(name)?.deleteFromRealm()
        }
    }

    fun changeDogName(nameOrigi: String, nameMod: String) {
        realm.executeTransaction {
            val dog = realm.where(Dog::class.java).equalTo("name", nameOrigi).findFirst()
            dog?.name = nameMod
        }
    }

    fun getPersonById(id: Int): Person {
        return realm.where(Person::class.java).equalTo("id", id).findFirst()!!
    }

    fun getPersonByName(name: String): Person? {
        return realm.where(Person::class.java).equalTo("name", name).findFirst()
    }

    fun getCatList(): RealmResults<Cat> {
        return realm.where(Cat::class.java).findAll().sort("name")
    }

    fun getCat(name: String): Cat? {
        return realm.where(Cat::class.java).equalTo("name", name).findFirst()
    }

    fun deleteCat(name: String) {
        realm.executeTransaction {
            getCat(name)?.deleteFromRealm()
        }
    }

    fun changeCatName(nameOrigi: String, nameMod: String) {
        realm.executeTransaction {
            val cat = realm.where(Cat::class.java).equalTo("name", nameOrigi).findFirst()
            cat?.name = nameMod
        }
    }

    fun deletePerson(name: String) {
        realm.executeTransaction {
            getPersonByName(name)?.deleteFromRealm()
        }
    }

}