package com.example.ftibi.kotlinrealm2.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.ftibi.kotlinrealm2.MakeSnackbarEvent
import com.example.ftibi.kotlinrealm2.RealmHelper
import com.example.ftibi.kotlinrealm2.R
import com.example.ftibi.kotlinrealm2.RxBus
import com.example.ftibi.kotlinrealm2.models.Dog
import io.realm.*

class DogFragment : Fragment() {

    lateinit var onRenameButtonClicked: CatFragment.OnRenameButtonClicked

    private lateinit var recyclerView: RecyclerView

    lateinit var realmHelper: RealmHelper

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is CatFragment.OnRenameButtonClicked) onRenameButtonClicked = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_animals2, container, false)

        realmHelper = RealmHelper(Realm.getDefaultInstance())

        recyclerView = v.findViewById(R.id.recycler_view_animals)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        registerForContextMenu(recyclerView)

        recyclerView.adapter = Adapter(realmHelper.getDogList())

        return v
    }

    inner class Adapter(data: OrderedRealmCollection<Dog>?) : RealmRecyclerViewAdapter<Dog, Adapter.ViewHolder>(data, true) {
        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindItems(this.getItem(position)!!)

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.cat_list_item, parent, false))
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener {
            init {
                itemView.setOnCreateContextMenuListener(this)
            }

            override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
                menu?.setHeaderTitle(dog.name)
                menu?.add(0, v!!.id, 0, getString(R.string.rename))?.setOnMenuItemClickListener {
                    onRenameButtonClicked.onRenameButtonClicked(dog.name!!, "dog")
                    true
                }
                menu?.add(0, v!!.id, 0, getString(R.string.delete))?.setOnMenuItemClickListener {
                    realmHelper.deleteDog(dog.name!!)
                    RxBus.publish(MakeSnackbarEvent(getString(R.string.dog_deleted)))
                    true
                }
            }

            private lateinit var dog: Dog

            fun bindItems(dog: Dog) {
                this.dog = dog
                itemView.findViewById<TextView>(R.id.name_cat_list_item).text = dog.name
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        unregisterForContextMenu(recyclerView)
    }

    interface OnRenameButtonClicked {
        fun onRenameButtonClicked(name: String, whichAnimal: String)
    }

}
