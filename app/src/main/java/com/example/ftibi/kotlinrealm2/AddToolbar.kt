package com.example.ftibi.kotlinrealm2

import android.app.Activity
import android.support.v7.widget.Toolbar
import android.view.View

class AddToolbar {
    companion object {
        fun addToolbar(
                activity: Activity,
                view: View?,
                id: Int,
                hasBackButton: Boolean,
                title: String? = activity.resources.getText(activity.resources.getIdentifier("app_name", "string", activity.packageName)).toString()
        ) {
            val toolbar = view?.findViewById<Toolbar>(id)
            (activity as FrameActivity).setSupportActionBar(toolbar)
            activity.supportActionBar?.setDisplayHomeAsUpEnabled(hasBackButton)
            activity.supportActionBar?.title = title
        }
    }
}