package com.example.ftibi.kotlinrealm2.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.ftibi.kotlinrealm2.AddToolbar
import com.example.ftibi.kotlinrealm2.RealmHelper
import com.example.ftibi.kotlinrealm2.R
import com.example.ftibi.kotlinrealm2.RxBus
import com.example.ftibi.kotlinrealm2.HideButtonEvent
import com.example.ftibi.kotlinrealm2.models.Cat
import io.realm.Realm
import io.realm.RealmList

class DetailsFragment : Fragment() {

    companion object {
        fun newInstance(id: Int): Fragment {
            val fragment = DetailsFragment()
            val bundle = Bundle()
            bundle.putInt("id_extra", id)
            fragment.arguments = bundle

            return fragment
        }
    }

    private lateinit var nameTextView: TextView
    private lateinit var ageTextView: TextView
    private lateinit var dogTextView: TextView
    private lateinit var catRecyclerView: RecyclerView

    private lateinit var realmHelper: RealmHelper

    override fun onStart() {
        super.onStart()

        setHasOptionsMenu(true)
        AddToolbar.addToolbar(activity, view, R.id.toolbar_details_fragment, true, getString(R.string.details))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_details, container, false)
        initViews(v)

        catRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)

        realmHelper = RealmHelper(Realm.getDefaultInstance())

        val person = realmHelper.getPersonById(arguments.getInt("id_extra"))

        catRecyclerView.adapter = CatAdapter(person.cats)

        nameTextView.text = person.name
        ageTextView.text = person.age.toString()
        dogTextView.text = person.dog?.name

        RxBus.publish(HideButtonEvent())

        return v
    }

    private fun initViews(v: View) {
        nameTextView = v.findViewById(R.id.name_details_fragment)
        ageTextView = v.findViewById(R.id.age_details_fragment)
        dogTextView = v.findViewById(R.id.dog_details_fragment)

        catRecyclerView = v.findViewById(R.id.cats_recycler_view)
    }

    inner class CatAdapter(catsList: RealmList<Cat>?) : RecyclerView.Adapter<CatAdapter.ViewHolder>() {
        private val catsList2 = catsList

        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindItems(catsList2?.get(position)!!)

        override fun getItemCount(): Int = catsList2?.size!!

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.cat_list_item, parent, false))
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bindItems(cat: Cat) {
                itemView.findViewById<TextView>(R.id.name_cat_list_item).text = cat.name
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                if (fragmentManager.backStackEntryCount > 0) fragmentManager.popBackStack()
            }
        }
        return super.onOptionsItemSelected(item)
    }

}