I made this app to practice, to learn Kotlin, Realm, Rx.
This app lets you create/delete Person, Cat and Dog objects. 
You can manage the animals by adding, removing and renaming them. You can add/remove Person objects which have a name and age and can have one Dog object and multiple Cat objects.

Screenshots:

Main page (MainFragment):
![](screenshots/people.png)

Add person (AddFragment):
![](screenshots/add_person.png)

Person's details (DetailsFragment):
![](screenshots/details_person.png)

Manage animals (ViewPagerFragment, CatFragment, DogFragment):
![](screenshots/manage_animals.png)

Add animal (AlertDialog):
![](screenshots/add_animal.png)